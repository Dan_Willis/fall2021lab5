public class NumberedCard extends Normal {

    public NumberedCard(Color color, int number){
        super(color, number);
    }

    public boolean canPlay(Card card){
        if (this.color == card.getColor()){
            return true;
        }else if(card instanceof Normal && this.number == ((Normal)card).getNumber()){
            return true;
        }else{
            return false;
        }
    }
}
