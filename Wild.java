public abstract class Wild implements Card {
    
    protected Color color;

    public Color getColor(){
        return this.color;
    }

    public void setColor(Color color){
        this.color = color;
    }
    
    public boolean canPlay(Card card){
        if (this instanceof Card){
            return true;
        }else{
            return false;
        }
    }
}
