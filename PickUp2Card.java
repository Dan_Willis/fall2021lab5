public class PickUp2Card extends Special{
    
    public PickUp2Card(Color color){
        super(color);
    }
    
    public boolean canPlay(Card card){
        if (this.color == card.getColor()){
            return true;
        }else if(card instanceof PickUp2Card){
            return true;
        }else{
            return false;
        }
    }    
}
