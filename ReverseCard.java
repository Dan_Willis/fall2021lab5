public class ReverseCard extends Special {
    
    public ReverseCard(Color color){
        super(color);
    }
    
    public boolean canPlay(Card card){
        if (this.color == card.getColor()){
            return true;
        }else if(card instanceof ReverseCard){
            return true;
        }else{
            return false;
        }
    }
}
