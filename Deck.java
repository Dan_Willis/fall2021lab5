import java.util.ArrayList;
import java.util.Random;

public class Deck {
    
    private ArrayList<Card> deck;

    //Constructor that creates a deck of 108 cards 
    public Deck(){
        this.deck = new ArrayList<Card>();
        // 4 wild cards
        int count = 0;
        while(count < 4){
            Card wildCard = new WildCard();
            this.deck.add(wildCard);
            count++;
        }
        count = 0;
        //4 wild pick up 4 cards
        while(count < 4){
            Card wildCardPickUp4Card = new WildPickUp4Card();
            this.deck.add(wildCardPickUp4Card);
            count++;
        }
        //72 Numbered cards (1-9)
        for(int i=0; i<4; i++){
            for(int j=1; j<10; j++){
                Card numberedCard1 = new NumberedCard(Color.values()[i], j);
                this.deck.add(numberedCard1);
                Card numberedCard2 = new NumberedCard(Color.values()[i], j);
                this.deck.add(numberedCard2);   
            }
        }
        //4 numbered card with the number ZERO
        for(int i=0; i<4; i++){
            Card numberedCard = new NumberedCard(Color.values()[i], 0);
            this.deck.add(numberedCard);  
        }
        //8 Skip Cards
        for(int i=0; i<4; i++){
            Card skipCard1 = new SkipCard(Color.values()[i]);
            this.deck.add(skipCard1);
            Card skipCard2 = new SkipCard(Color.values()[i]);
            this.deck.add(skipCard2);   
        }
        //8 Pick up 2 cards
        for(int i=0; i<4; i++){
            Card pickUp2Card1 = new PickUp2Card(Color.values()[i]);
            this.deck.add(pickUp2Card1);
            Card pickUp2Card2 = new PickUp2Card(Color.values()[i]);
            this.deck.add(pickUp2Card2);   
        }
        //8 Reverse cards
        for(int i=0; i<4; i++){
            Card reverseCard1 = new ReverseCard(Color.values()[i]);
            this.deck.add(reverseCard1);
            Card reverseCard2 = new ReverseCard(Color.values()[i]);
            this.deck.add(reverseCard2);   
        }
    }

    public void addToDeck(Card card){
        this.deck.add(0, card);
        
    }

    public Card draw(){
        Card topCard = this.deck.get(deck.size()-1);
        this.deck.remove(deck.size()-1);
        return topCard;
    }

    public void shuffle(){
        int length = this.deck.size();
        Random random = new Random();
        for(int i=0; i < length; i++){
            int randCard = random.nextInt(length - 1);
            swap(i, randCard);
        }   
    }

    public void swap(int position, int randCard){
        Card holder = this.deck.get(position);
        this.deck.set(position, this.deck.get(randCard));
        this.deck.set(randCard, holder);
    }

    public int getLength(){
        return this.deck.size();
    }

    public ArrayList<Card> getCards(){
        return this.deck;
    }
}




