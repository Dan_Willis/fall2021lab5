import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestUno {
    @Test
    public void testCreateWildCard(){
        Card wildCard = new WildCard();
        assertEquals(null, wildCard.getColor());
    }

    @Test
    public void testCanPlayWildCard(){
        Card wildCard = new WildCard();
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertTrue(wildCard.canPlay(numberedCard));
    }

    @Test
    public void testCreateWildPickUp4Card(){
        Card wildPickUp4Card = new WildPickUp4Card();
        assertEquals(null, wildPickUp4Card.getColor());
    }

    @Test
    public void testCanPlayWildPickUp4Card(){
        Card wildPickUp4Card = new WildPickUp4Card();
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertTrue(wildPickUp4Card.canPlay(numberedCard));
    }

    @Test 
    public void testWildCardSetColor(){
        Card wildCard = new WildCard();
        ((Wild)wildCard).setColor(Color.GREEN);
        assertEquals(Color.GREEN, wildCard.getColor());
    }

    @Test 
    public void testWildPickUp4CardSetColor(){
        Card wildCard = new WildCard();
        ((Wild)wildCard).setColor(Color.GREEN);
        assertEquals(Color.GREEN, wildCard.getColor());
    }

    @Test
    public void testCreateSkipCard(){
        Card skipCard = new SkipCard(Color.BLUE);
        assertEquals(Color.BLUE, skipCard.getColor());
    }

    @Test
    public void testCanPlaySkipCard(){
        Card skipCard = new SkipCard(Color.BLUE);
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertTrue(skipCard.canPlay(numberedCard));
    }

    @Test
    public void testCanNotPlaySkipCard(){
        Card skipCard = new SkipCard(Color.BLUE);
        Card numberedCard = new NumberedCard(Color.RED, 9);
        assertFalse(skipCard.canPlay(numberedCard));
    }
    
    @Test
    public void testCreateReverseCard(){
        Card reverseCard = new ReverseCard(Color.RED);
        assertEquals(Color.RED, reverseCard.getColor());
    }

    @Test
    public void testCanPlayReverseCard(){
        Card reverseCard = new ReverseCard(Color.BLUE);
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertTrue(reverseCard.canPlay(numberedCard));
    }

    @Test
    public void testCanNotPlayReverseCard(){
        Card reverseCard = new ReverseCard(Color.RED);
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertFalse(reverseCard.canPlay(numberedCard));
    }

    @Test
    public void testCreatePickUp2Card(){
        Card pickUp2Card = new PickUp2Card(Color.GREEN);
        assertEquals(Color.GREEN, pickUp2Card.getColor());
    }

    @Test
    public void testCanPlayPickUp2Card(){
        Card pickUp2Card = new PickUp2Card(Color.BLUE);
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertTrue(pickUp2Card.canPlay(numberedCard));
    }

    @Test
    public void testCanNotPlayPickUp2Card(){
        Card pickUp2Card = new PickUp2Card(Color.GREEN);
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertFalse(pickUp2Card.canPlay(numberedCard));
    }

    @Test
    public void testCreateNumberedCard(){
        Card numberedCard = new NumberedCard(Color.BLUE, 9);
        assertEquals(Color.BLUE, numberedCard.getColor());
        assertEquals(9, ((Normal)numberedCard).getNumber());
    }

    @Test
    public void testCanPlayNumberedCardSameColor(){
        Card numberedCard1 = new NumberedCard(Color.BLUE, 1);
        Card numberedCard2 = new NumberedCard(Color.BLUE, 9);
        assertTrue(numberedCard1.canPlay(numberedCard2));
    }

    @Test
    public void testCanPlayNumberedCardSameNumber(){
        Card numberedCard1 = new NumberedCard(Color.GREEN, 9);
        Card numberedCard2 = new NumberedCard(Color.BLUE, 9);
        assertTrue(numberedCard1.canPlay(numberedCard2));
    }

    @Test
    public void testCanNotPlayNumberedCard(){
        Card numberedCard1 = new NumberedCard(Color.GREEN, 4);
        Card numberedCard2 = new NumberedCard(Color.BLUE, 9);
        assertFalse(numberedCard1.canPlay(numberedCard2));
    }

    @Test
    public void testNumberOfCardsInDeck(){
        Deck deck = new Deck();
        //There is 108 cards in a deck of Uno
        assertEquals(108, deck.getLength());
    }

    @Test
    public void testAddToDeck(){
        Deck deck = new Deck();
        Card card = new NumberedCard(Color.RED, 3);
        deck.addToDeck(card);
        assertEquals(card, deck.getCards().get(0));
    }

    @Test
    public void testDraw(){
        Deck deck = new Deck();
        //check the original length of the deck
        assertEquals(108, deck.getLength());
        Card topCard = deck.getCards().get(deck.getLength()-1);
        //check if the top card is the same as the return value of the draw() method.
        assertEquals(topCard, deck.draw());
        //check if the top card has been removed from the deck (108-1= 107)
        assertEquals(107, deck.getLength());
    }

    @Test
    public void testSizeAfterShuffle(){
        //check if the shuffle() method doesn't affect the size of the deck
        Deck deck = new Deck();
        deck.shuffle();
        assertEquals(108, deck.getLength());
    }
}
