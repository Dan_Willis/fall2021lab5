public abstract class Special implements Card {
    
    protected Color color;

    public Special(Color color){
        this.color = color;
    }

    public Color getColor(){
        return this.color;
    }
}
