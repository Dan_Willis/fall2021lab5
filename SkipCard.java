public class SkipCard extends Special {

    public SkipCard(Color color){
        super(color);
    }
    
    public boolean canPlay(Card card){
        if (this.color == card.getColor()){
            return true;
        }else if(card instanceof SkipCard){
            return true;
        }else{
            return false;
        }
    }
}
