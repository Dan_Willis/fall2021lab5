public abstract class Normal implements Card {
    
    protected Color color;
    protected int number;

    public Normal(Color color, int number){
        this.color = color;
        this.number = number;
    }
    
    public Color getColor(){
        return this.color;
    }

    public int getNumber(){
        return this.number;
    }
}
