public interface Card{
    public boolean canPlay(Card card);
    public Color getColor();
}